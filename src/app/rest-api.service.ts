import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';

import { catchError, tap, map } from 'rxjs/operators';
import { promise } from 'protractor';
import { AppModule } from '../app/app.module';

const isCloud: boolean = false;
//Production MCS
const cloudURL= "https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443/mobile/custom/IssueTracking_API";
const authorizationCloud: any= 'Basic RkFFRTVDQkI4QjQyNDhGNEJEQjdDMEIwMDQzRDYxRDlfTW9iaWxlQW5vbnltb3VzX0FQUElEOjJiYjcxZDg4LTZiZGItNDZiZi04YzM1LTI2OGFlYzgwNTUyMQ==';
const OracleMobileBackendId: any ='62a27650-c5f8-43a8-a76e-b16c900a443f';
const accessControlAllowOrigin: string = "http://localhost:8080";

//Test MCS
// const cloudURL= "https://FAEE5CBB8B4248F4BDB7C0B0043D61D9.mobile.ocp.oraclecloud.com:443/mobile/custom/IssueTracking_API_TEST";
// const authorizationCloud: any= 'Basic RkFFRTVDQkI4QjQyNDhGNEJEQjdDMEIwMDQzRDYxRDlfTW9iaWxlQW5vbnltb3VzX0FQUElEOjJiYjcxZDg4LTZiZGItNDZiZi04YzM1LTI2OGFlYzgwNTUyMQ==';
// const OracleMobileBackendId: any ='9a2da9f1-f352-438c-8203-590faf0554e0';

// const apiUrlRequest = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/getRequests";
// const apiUrlPrimaryAssignee = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/getPrimaryAssignee";
// const apiUrlSecondaryAssignee = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/getSecondaryAssignee";
// const apiUrlProjectType = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/getProjectsTypeList";
// const apiUrlPriority = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/getPriorityList";
// const apiUrlProjectPhase = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/getProjectsPhaseList";
// const apiUrlAssignRequest = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/submitAssignRequest";
// const apiUrlLogin = "http://10.10.131.34:7003/issuetracking/resources/issuetracking/doLogin";

const apiUrlRequest = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/getRequests";
const apiUrlPrimaryAssignee = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/getPrimaryAssignee";
const apiUrlSecondaryAssignee = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/getSecondaryAssignee";
const apiUrlProjectType = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/getProjectsTypeList";
const apiUrlPriority = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/getPriorityList";
const apiUrlProjectPhase = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/getProjectsPhaseList";
const apiUrlAssignRequest = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/submitAssignRequest";
const apiUrlLogin = "http://127.0.0.1:7101/issuetracking/resources/issuetracking/doLogin";

//  //Test JCS
// const apiUrlRequest = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/getRequests";
// const apiUrlPrimaryAssignee = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/getPrimaryAssignee";
// const apiUrlProjectType = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/getProjectsTypeList";
// const apiUrlPriority = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/getPriorityList";
// const apiUrlProjectPhase = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/getProjectsPhaseList";
// const apiUrlAssignRequest = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/submitAssignRequest";
// const apiUrlLogin = "https://jcsapp-mbccloudservices.jcs.ocp.oraclecloud.com/issuetracking/resources/issuetracking/doLogin";


//  // Prod JCS
// const apiUrlRequest = "http://130.61.62.207/issuetracking/resources/issuetracking/getRequests";
// const apiUrlPrimaryAssignee = "http://130.61.62.207/issuetracking/resources/issuetracking/getPrimaryAssignee";
// const apiUrlSecondaryAssignee = "http://130.61.62.207/issuetracking/resources/issuetracking/getSecondaryAssignee";
// const apiUrlProjectType = "http://130.61.62.207/issuetracking/resources/issuetracking/getProjectsTypeList";
// const apiUrlPriority = "http://130.61.62.207/issuetracking/resources/issuetracking/getPriorityList";
// const apiUrlProjectPhase = "http://130.61.62.207/issuetracking/resources/issuetracking/getProjectsPhaseList";
// const apiUrlAssignRequest = "http://130.61.62.207/issuetracking/resources/issuetracking/submitAssignRequest";
// const apiUrlLogin = "http://130.61.62.207/issuetracking/resources/issuetracking/doLogin";

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  targetURL: any;
  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    // console.log('pMsg:'+pMsg);
    
    //console.log(error.error);
    console.log(error);
    console.log(error.error);
    console.log(ErrorEvent);
    //console.log(ErrorEvent);

    if (error.error instanceof ErrorEvent) {
     
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        'Backend returned code ${error.status}, ' +
        'body was: ${error.error}');
        alert(error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

 

  doLoginx(pUserName: any, pPassword: any) {
    console.log('doLoginx 22222222 a3');



let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
    headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/doLogin');
    }

    headersX = headersX.append('pUserName',pUserName);
    headersX = headersX.append('pPassword',pPassword);
    
    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlLogin;

    this.targetURL =  AppModule.wsURL+"/issuetracking/doLogin";

return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
 map(this.extractData),
//);
  catchError(this.handleError));
       
  }

  getRequestList(): Observable<any> {
    console.log('getRequestList');
    // let vx;
    // const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;',
    //   'Access-Control-Allow-Origin':'*',
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE='
    //   'Authorization':authorizationCloud,
    // 'Oracle-Mobile-Backend-Id':OracleMobileBackendId,
    // 'resourceName':'issuetracking/getRequests'
    //   })
    // };
    
    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
    headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/getRequests');
    }

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlRequest;

    this.targetURL = AppModule.wsURL+"/issuetracking/getRequests";
    
    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  getPrimaryAssigneeList(): Observable<any> {
    console.log('getPrimaryAssigneeList');
    let vx;
    //   const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;',
    //   'Access-Control-Allow-Origin':'*'
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE='
    //   })
    // };

    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/getPrimaryAssignee');
    }

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlPrimaryAssignee;

    this.targetURL = AppModule.wsURL+"/issuetracking/getPrimaryAssignee";

    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  getSecondaryAssigneeList(): Observable<any> {
    console.log('getSecondaryAssigneeList');
    let vx;
    //   const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;',
    //   'Access-Control-Allow-Origin':'*'
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE='
    //   })
    // };

    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
    headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/getSecondaryAssignee');
    }

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlSecondaryAssignee;

    this.targetURL = AppModule.wsURL+"/issuetracking/getSecondaryAssignee";

    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }
    getProjectsPhaseList(): Observable<any> {
      console.log('getProjectsPhaseList');
    let vx;
    //   const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
    //   'Access-Control-Allow-origin':'*' //,
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE='
    //   })
    // };

    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/getProjectsPhaseList');
    }

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlProjectPhase;
    this.targetURL = AppModule.wsURL+"/issuetracking/getProjectsPhaseList";
    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  getProjectsTypeList(): Observable<any> {
    console.log('getProjectsTypeList');
    let vx;
    //   const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
    //   'Access-Control-Allow-origin':'*'
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE='
    //   })
    // };

    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
    headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/getProjectsTypeList');
    }

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlProjectType;

    this.targetURL = AppModule.wsURL+"/issuetracking/getProjectsTypeList";

    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  getPriorityList(): Observable<any> {
    console.log('getPriorityList');
    let vx;
    //   const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
    //   'Access-Control-Allow-origin':'*'
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE='
    //   })
    // };

    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
headersX = headersX.append('Authorization',AppModule.authentication);

    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/getPriorityList');
    }

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlPriority;
          
    this.targetURL = AppModule.wsURL+"/issuetracking/getPriorityList";

    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  submitAssignmentRequest(pProjectId: any,
    pPrimaryAssigneeId: any,
    pSecondaryAssigneeId: any,
    pUserName: any,
    pProjectType: any,
    pProjectStatus: any,
    pProjectPhase: any,
    pPrimaryStartDate: any,
    pPriority: any
    ): Observable<any> {
      console.log('getPriorityList3');
      let vx;
    //   const httpOptionsxx = {
    //   headers: new HttpHeaders({
    //   'Content-Type':'application/json ; charset=UTF-8' ,
    //   'Accept':'application/json',
    //   'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
    //   'Access-Control-Allow-origin':'*',
    //   // 'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
    //   pProjectId: pProjectId+"",
    //   pPrimaryAssigneeId: pPrimaryAssigneeId+"",
    //   pSecondaryAssigneeId: pSecondaryAssigneeId+"",
    //   pUserName: pUserName,
    //   pProjectType: pProjectType,
    //   pProjectStatus: pProjectStatus,
    //   pProjectPhase: pProjectPhase,
    //   pPriority: pPriority,
    //   pPrimaryStartDate: pPrimaryStartDate
    //   })
    // };


    let headersX: HttpHeaders = new HttpHeaders();
    headersX = headersX.append('Content-Type','application/json ; charset=UTF-8');
    headersX = headersX.append('Accept','application/json');
    headersX = headersX.append('Access-Control-Allow-Headers','Allow-Headers,Content-Type;Access-Control-Allow-origin;Authorization;');
    headersX = headersX.append('Access-Control-Allow-Origin',accessControlAllowOrigin);
    headersX = headersX.append('Authorization',AppModule.authentication);
    
    if (isCloud)
    {
    headersX = headersX.append('Authorization',authorizationCloud);
    headersX = headersX.append('Oracle-Mobile-Backend-Id',OracleMobileBackendId);
    headersX = headersX.append('resourceName','issuetracking/submitAssignRequest');
    }

    headersX = headersX.append('pProjectId',pProjectId+"");
    headersX = headersX.append('pPrimaryAssigneeId',pPrimaryAssigneeId+"");
    headersX = headersX.append('pSecondaryAssigneeId',pSecondaryAssigneeId+"");
    headersX = headersX.append('pUserName',pUserName);
    // headersX = headersX.append('pProjectType',pProjectType);
    // headersX = headersX.append('pProjectStatus',pProjectStatus);
    // headersX = headersX.append('pProjectPhase',pProjectPhase);
    headersX = headersX.append('pPriority',pPriority);
    // headersX = headersX.append('pPrimaryStartDate',pPrimaryStartDate);

    const httpOptionsxx = {
      headers: headersX
    };

    // if (isCloud)
    //   this.targetURL = cloudURL;
    // else 
    //   this.targetURL = apiUrlAssignRequest;

    this.targetURL = AppModule.wsURL+"/issuetracking/submitAssignRequest";

    return this.http.post(this.targetURL,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

     ////////////////// IDCS

     async intiateAuth( user :string , userPass: string , MfaValue :string)
     {
     
     
     
       let url=AppModule.authURL+'/app/session/authenticate'
     
       let tok  :any= await this.getToken();
       let at;
       if (tok)
       at=tok.access_token;
       let methodHeaders ={};
       methodHeaders['Authorization']='Bearer '+at;
       methodHeaders['Accept']='application/json';
       methodHeaders['Content-Type']='application/json';
      // methodHeaders['Access-Control-Allow-Origin']='*';
      // methodHeaders['Origin']='http://localhost:8100';
       
      //  let loader=AppModule.showLoader(this.loadingController );
     
       let body=null;
     
       let body1 = {
         "username":user,
         "password": userPass,
         "state":AppModule.state+""
         };
     
         

         let body2 = {
           "MFAvalue":MfaValue,
           "nextOp": AppModule.nextOp,
           "requestState":AppModule.requestState+"",
           "nextAuthFactors":AppModule.nextAuthFactors+"",
           "state":AppModule.state+""
           };
     
     
         if (AppModule.state==1)
             body=body1;
         
         if (AppModule.state==3)
             body=body2; 
       
      //  if (AppModule.mode==0)
      //  {
     
       return new Promise(resolve => {
         this.http.post(url, body,{ headers: methodHeaders }).subscribe(data => {
             resolve(data);
            // AppModule.stopLoader(loader)
           }
           , err => {
     
           console.log(err);
             if (err.status==401)
             alert("Erorr 104, Please try again or contact the system administrator");
            //  AppModule.showMessage(this.AlertController,"Erorr 104, Please try again or contact the system administrator");
             else
            //  AppModule.showMessage(this.AlertController,err.message);
            //  AppModule.stopLoader(loader); 
             alert(err.message)
           }
         );
         });
     
      //  }
    //    else
    //    {
    //  this.http.setDataSerializer('json') 
    //   return new Promise(resolve => {
    //         this.http.post(url, JSON.parse(JSON.stringify(body)), methodHeaders).then(data => {
    //          resolve(JSON.parse(data.data));
           
    //          }
    //          , err => {
    //        alert(err)
           
    //          }
    //        );
    //        });
         
    //    }
     
       }
     
     
       getToken( )
       {
       
         let url =AppModule.tokURL+'/oauth2/v1/token';
       
         let methodHeaders ={};
     
         methodHeaders['Authorization']='Basic '+AppModule.tokAuth;
         methodHeaders['Content-Type']='application/x-www-form-urlencoded';
        //  let loader=AppModule.showLoader(this.loadingController );
     
       
       let body='scope=urn:opc:idm:__myscopes__&grant_type=client_credentials';
          
       
         return new Promise(resolve => {
           this.http.post(url, body,{ headers: methodHeaders }).subscribe(data => {
           // alert (data);
               resolve(data);
              // AppModule.stopLoader(loader)
             }
             , err => {
               //AppModule.handleError(err,this.loadingController,this.AlertController,this.appCtrl);
              //  AppModule.stopLoader(loader)
         alert(err.message);  
             }
           );
           });
       
       
         }
     

}
