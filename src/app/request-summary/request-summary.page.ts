import { Component, OnInit , ViewChild} from '@angular/core';
import { RestApiService } from '../rest-api.service';
import {AppModule} from '../app.module';
import { IonInfiniteScroll } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-request-summary',
  templateUrl: './request-summary.page.html',
  styleUrls: ['./request-summary.page.scss'],
})


export class RequestSummaryPage implements OnInit {
  @ViewChild(IonInfiniteScroll,null) infiniteScroll: IonInfiniteScroll;

  blankPage: any;
  requestList: any = [{}];
  searchTerm : any="";
  stp : any =10;

  constructor(public appModule: AppModule
             ,public restApiService: RestApiService 
             ,public loadingController: LoadingController
             ,public toastController: ToastController
             ,public router: Router
            //  ,public infiniteScroll: IonInfiniteScroll
            
             ) { 
              
              this.requestList = [{}];
             }

             loadData(event) {
              setTimeout(() => {
                console.log('Done');
                this.stp = this.stp +10;

                this.requestList=AppModule.requestList.slice(0,this.stp);
                event.target.complete();
          
                // App logic to determine if all data is loaded
                // and disable the infinite scroll
                // if (1 == 1) {
                //   event.target.disabled = true;
                // }
              }, 500);
            }
          
            toggleInfiniteScroll() {
              this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
            }


             ionViewWillEnter()
             {
  
              if (AppModule.primaryAssigneeList[0] != null)
                  console.log('primary assignee list already pulled');
              else
                this.getPrimaryAssigneeList();
              

              if (AppModule.secondaryAssigneeList[0] != null)
                  console.log('secondary assignee list already pulled');
              else
                 this.getSecondaryAssigneeList();

              // this.getProjectPhaseList();
              // this.getProjectsTypeList();

              if (AppModule.priorityList[0] != null)
                   {
                     console.log('priority list already pulled');
                   }
                   else
                  this.getPriorityList();

                  this.getRequestList();
                  this.searchTerm=null;
                } 

        doInfinite(infiniteScroll) {
        try{
          this.requestList=AppModule.requestList.slice(0,11);
          infiniteScroll.complete();
        }
        catch (err)
        {
          console.log(err);
        }
        }

             filterItems(searchTerm) {
              // this.requestList = AppModule.requestList;
              return AppModule.requestList.filter(item => {
                this.requestList = item.project_number.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
                this.requestList = this.requestList + item.project_description.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;

                return this.requestList;
              });
            }   

  setFilteredItems() {
    //  alert('Filetring: '+this.searchTerm);  
     
    // if (this.searchTerm != '')
    // {

      // this.getRequestList();

      if (AppModule.requestList[0] != null)
      {
      this.requestList = 
      this.filterItems(this.searchTerm);
    }
    // }
    //  else
    //  {
    //   alert('blank');
    //   this.getRequestList();


    //  }

    }

openAssignForm(pdata:any)
  {
    
    console.log('calling openAssignForm:');
   
    AppModule.requestDetails = pdata;
       this.router.navigateByUrl('/assign-request');
   
  }

  logout()
  {
// alert('logout');
this.router.navigateByUrl('/login'); 

  }
    async getPrimaryAssigneeList() {
          console.log('inside async getPrimaryAssignee()');
              const loading = await this.loadingController.create({
                message:'Pulling primary assignee...'    });
              await loading.present();
              console.log("before invoking ws: ");
              await this.restApiService.getPrimaryAssigneeList()
              .subscribe(res => {
                  console.log('before list');
                  console.log(res);
                  console.log('after list');
                  //this.requestList = res;
                  if (res[0] == null)
                  {
                    console.log('no data');
                  }
                       else
                       {
                       
                        AppModule.primaryAssigneeList = res;
                  console.log('there is data 1');

                  if (AppModule.primaryAssigneeList[0] != null)
                  {
                    if (AppModule.primaryAssigneeList[0].Status == 'E'){
                    alert('Unable to pull primary assignee: '+AppModule.primaryAssigneeList[0].Message);
                    this.logout();
                    }
                  }
                }
                  loading.dismiss();
                }, err => {
                  // alert('kkkkk');
                  console.log(err);
                  loading.dismiss();
                });
             }
      
             async getSecondaryAssigneeList() {
              console.log('inside async getSecondaryAssigneeList()');
                  const loading = await this.loadingController.create({
                    message:'Pulling primary assignee...'    });
                  await loading.present();
                  console.log("before invoking ws: ");
                  await this.restApiService.getSecondaryAssigneeList()
                  .subscribe(res => {
                      console.log('before list');
                      console.log(res);
                      console.log('after list');
                      //this.requestList = res;
                      if (res[0] == null)
                      {
                        console.log('no data');
                      }
                           else
                           {
                           
                            AppModule.secondaryAssigneeList = res;
                      console.log('there is data 2');

                      if (AppModule.secondaryAssigneeList[0] != null)
                      {
                        if (AppModule.secondaryAssigneeList[0].Status == 'E'){
                        alert('Unable to pull Secondary assignee: '+AppModule.secondaryAssigneeList[0].Message);
                        this.logout();
                        }
                      }

                           }
                      loading.dismiss();
                    }, err => {
                      // alert('kkkkk');
                      console.log(err);
                      loading.dismiss();
                    });
                 }

             async getProjectPhaseList() {
              console.log('inside async getProjectPhaseList()');
                  const loading = await this.loadingController.create({
                    message:'Pulling project phase...'    });
                  await loading.present();
                  console.log("before invoking ws: ");
                  await this.restApiService.getProjectsPhaseList()
                  .subscribe(res => {
                      console.log('before list');
                      console.log(res);
                      console.log('after list');
                      //this.requestList = res;
                      if (res[0] == null)
                      {
                        console.log('no data');
                      }
                           else
                           {
                           
                            AppModule.projectPhaseList = res;
                      console.log('there is data 3');
                           }
                      loading.dismiss();
                    }, err => {
                        console.log(err);
                      loading.dismiss();
                    });
                 }

             async getRequestList() {
              console.log('inside async getRequestList()');
                  const loading = await this.loadingController.create({
                    message:'Pulling request details...'    });
                  await loading.present();
                  console.log("before invoking ws: ");
                  // alert('before invoke getRequestList');
                  await this.restApiService.getRequestList()
                  .subscribe(res => {
                      // console.log('before list');
                      // console.log(res);
                      console.log('after list');
                      
                      this.requestList = res;
                      if (res[0] == null)
                      {
                        this.blankPage=true;
                        console.log('no data');
                        // alert('there is no data');
                      }
                           else
                           {
                            // alert('data exists');
                            this.blankPage=false;
                            AppModule.requestList = res;

                            // AppModule.requestList=AppModule.requestList.slice(0,10);

                            this.requestList = res.slice(0,10);
                      // console.log(this.requestList);
                      //  console.log('appModule variable printed');
              
                      if (AppModule.requestList[0] != null)
                      {
                        if (AppModule.requestList[0].Status == 'E'){
                        alert('Unable to pull requests: '+AppModule.requestList[0].Message);
                        this.logout();
                        }
                      }
                    }
                      //blankPage
                      loading.dismiss();
                      //this.sendSMS();
                    }, err => {
                      alert('err:'+err);
                      console.log(err);
                      loading.dismiss();
                    });

                    
                 }    

                 async getPriorityList() {
                  console.log('inside async getPriorityList()');
                      const loading = await this.loadingController.create({
                        message:'Pulling priority list...'    });
                      await loading.present();
                      console.log("before invoking ws: ");
                      await this.restApiService.getPriorityList()
                      .subscribe(res => {
                          console.log('before getPriorityList');
                          console.log(res);
                          console.log('after getPriorityList');
                          //this.requestList = res;
                          if (res[0] == null)
                          {
                            console.log('no data');
                          }
                               else
                               {
                                AppModule.priorityList = res;
                          console.log('there is data 4');

                          if (AppModule.priorityList[0] != null)
                      {
                        if (AppModule.priorityList[0].Status == 'E'){
                        alert('Unable to pull priority list: '+AppModule.priorityList[0].Message);
                        this.logout();
                        }
                      }

                               }
                          loading.dismiss();
                        }, err => {
                          console.log(err);
                          loading.dismiss();
                        });
                     }
                         
                     async getProjectsTypeList() {
                      console.log('inside async getProjectsTypeList()');
                          const loading = await this.loadingController.create({
                            message:'Pulling project type list...'    });
                          await loading.present();
                          console.log("before invoking ws: ");
                          await this.restApiService.getProjectsTypeList()
                          .subscribe(res => {
                              console.log('before list');
                              console.log(res);
                              console.log('after list');
                              //this.requestList = res;
                              if (res[0] == null)
                              {
                                console.log('no data');
                              }
                                   else
                                   {
                                    AppModule.projectTypeList = res;
                              console.log('there is data 5');
                                   }
                              loading.dismiss();
                            }, err => {
                              console.log(err);
                              loading.dismiss();
                            });
                         }

                         ngOnInit() {
  }

}
