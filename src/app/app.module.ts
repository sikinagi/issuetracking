import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule,IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    FingerprintAIO,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  static requestList: any = [];
  static primaryAssigneeList: any = [];
  static secondaryAssigneeList: any = [];
  static userName: string = "AIDRAI";
  static priorityList: any = [];
//   static projectsType: any = [];
  static projectPhaseList: any = [];
  static projectTypeList: any = [];
  static requestDetails: any = {};
  static state : any = '1';
  static status :any = 0;

  static wsURL='https://erpapp.mymbc.net/mbcWebservice_IDCS/resources';
static authentication  ='';
static isCloud=false;
static backendId='';

/// idcs
static authURL= 'https://erpapp.mymbc.net/login/services';
//static authURL= 'http://localhost:7101/login/resources';
//static authURL= 'http://10.10.131.34:7003/login/services';
static tokURL  ='https://idcs-5fdca24dfdab4263b0f2341aebde4d78.identity.oraclecloud.com';
static tokAuth='YjgwZDIyZDRiMTk5NDBkNDhmOTgwOWFiYThjZjZhNTA6Y2UwNDk1NTYtNTRiMy00NTg5LTllZDUtMjdkNDVjOWRiZGE4';

static nextOp = '';
         static requestState = '';
         static nextAuthFactors = '';

  constructor()
  {

AppModule.requestList=[{}];
AppModule.state='';
    /*
    AppModule.requestList=[
      {
          "submission_date": "15-JUL-2019",
          "project_description": "Application not working",
          "priority_color": "normalPriority",
          "project_number": "MBC190711021",
          "project_id": 11021,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "18-JUL-2019",
          "project_description": "Tes100000092",
          "priority_color": "normalPriority",
          "project_number": "MBC190711052",
          "project_id": 11052,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Urgent"
      },
      {
          "submission_date": "22-JUL-2019",
          "project_description": "sdfsdf",
          "priority_color": "normalPriority",
          "project_number": "MBC190711066",
          "project_id": 11066,
          "preparor_id": 47282,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Rafik Abdelmonsef Mehaney Beshara",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "23-JUL-2019",
          "project_description": "23-JUL",
          "priority_color": "normalPriority",
          "project_number": "MBC190711070",
          "project_id": 11070,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "17-JUL-2019",
          "project_description": "TEST 1111",
          "priority_color": "normalPriority",
          "project_number": "MBC190711050",
          "project_id": 11050,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "11-JUL-2019",
          "project_description": "sdffsd",
          "priority_color": "normalPriority",
          "project_number": "MBC190711002",
          "project_id": 11002,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "11-JUL-2019",
          "project_description": "sdfsdfsd",
          "priority_color": "normalPriority",
          "project_number": "MBC190711003",
          "project_id": 11003,
          "preparor_id": 33398,
          "module": "HR",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "11-JUL-2019",
          "project_description": "ttttttttttttttttt",
          "priority_color": "normalPriority",
          "project_number": "MBC190711004",
          "project_id": 11004,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "11-JUL-2019",
          "project_description": "Unable to create Requisition",
          "priority_color": "normalPriority",
          "project_number": "MBC190711005",
          "project_id": 11005,
          "preparor_id": 33398,
          "module": "PROCUREMENT",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "11-JUL-2019",
          "project_description": "sdfsd",
          "priority_color": "normalPriority",
          "project_number": "MBC190711006",
          "project_id": 11006,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "17-JUL-2019",
          "project_description": "TST 1003",
          "priority_color": "normalPriority",
          "project_number": "MBC190711051",
          "project_id": 11051,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "18-JUL-2019",
          "project_description": "Test K01",
          "priority_color": "normalPriority",
          "project_number": "MBC190711053",
          "project_id": 11053,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Normal"
      },
      {
          "submission_date": "10-JUL-2019",
          "project_description": "Issue test",
          "priority_color": "normalPriority",
          "project_number": "MBC190710985",
          "project_id": 10985,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "15-JUL-2019",
          "project_description": "AP Issue 10001#",
          "priority_color": "highPriority",
          "project_number": "MBC190711041",
          "project_id": 11041,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "High"
      },
      {
          "submission_date": "10-JUN-2019",
          "project_description": "Error in AP approval",
          "priority_color": "normalPriority",
          "project_number": "MBC190610941",
          "project_id": 10941,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "New",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Normal"
      },
      {
          "submission_date": "10-JUL-2019",
          "project_description": "Test notification",
          "priority_color": "normalPriority",
          "project_number": "MBC190710987",
          "project_id": 10987,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "17-JUL-2019",
          "project_description": "Test 17 Jul",
          "priority_color": "normalPriority",
          "project_number": "MBC190711048",
          "project_id": 11048,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "17-JUL-2019",
          "project_description": "TET 1002",
          "priority_color": "normalPriority",
          "project_number": "MBC190711049",
          "project_id": 11049,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "16-JUL-2019",
          "project_description": "bbbbbbbb",
          "priority_color": "normalPriority",
          "project_number": "MBC190711043",
          "project_id": 11043,
          "preparor_id": 1520,
          "module": "HR",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "29-MAY-2019",
          "project_description": "This is test issue 29-May",
          "priority_color": "highPriority",
          "project_number": "MBC190510901",
          "project_id": 10901,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "High"
      },
      {
          "submission_date": "09-JUN-2019",
          "project_description": "Unable to open form.",
          "priority_color": "normalPriority",
          "project_number": "MBC190610921",
          "project_id": 10921,
          "preparor_id": 1520,
          "module": "HR",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Normal"
      },
      {
          "submission_date": "10-JUL-2019",
          "project_description": "Test",
          "priority_color": "normalPriority",
          "project_number": "MBC190710982",
          "project_id": 10982,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "17-JUL-2019",
          "project_description": "TST 1001",
          "priority_color": "normalPriority",
          "project_number": "MBC190711046",
          "project_id": 11046,
          "preparor_id": 1520,
          "module": "PROJECTS",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "28-JUL-2019",
          "project_description": "3- test",
          "priority_color": "normalPriority",
          "project_number": "MBC190711101",
          "project_id": 11101,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "16-JUL-2019",
          "project_description": "werewr",
          "priority_color": "normalPriority",
          "project_number": "MBC190711042",
          "project_id": 11042,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Configuration",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "16-JUL-2019",
          "project_description": "sdfsdf",
          "priority_color": "normalPriority",
          "project_number": "MBC190711044",
          "project_id": 11044,
          "preparor_id": 1520,
          "module": "HR",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "27-AUG-2019",
          "project_description": "Test 27 Aug",
          "priority_color": "highPriority",
          "project_number": "MBC190811161",
          "project_id": 11161,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "High"
      },
      {
          "submission_date": "10-JUN-2019",
          "project_description": "AP Report Error",
          "priority_color": "normalPriority",
          "project_number": "MBC190610942",
          "project_id": 10942,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "New",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Normal"
      },
      {
          "submission_date": "10-JUN-2019",
          "project_description": "Error in AP approval issue",
          "priority_color": "normalPriority",
          "project_number": "MBC190610943",
          "project_id": 10943,
          "preparor_id": 28624,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "New",
          "preparor": "Praveen Kamtala",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "22-JUL-2019",
          "project_description": "Test 22JUL",
          "priority_color": "normalPriority",
          "project_number": "MBC190711067",
          "project_id": 11067,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "10-JUL-2019",
          "project_description": "sdfsdfsdfsdf",
          "priority_color": "normalPriority",
          "project_number": "MBC190710988",
          "project_id": 10988,
          "preparor_id": 33398,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": null,
          "preparor": "Aissam Drai",
          "message": "1",
          "priority": "Low"
      },
      {
          "submission_date": "21-JUL-2019",
          "project_description": "3- test",
          "priority_color": "normalPriority",
          "project_number": "MBC190711065",
          "project_id": 11065,
          "preparor_id": 1520,
          "module": "FINANCE",
          "project_status": "Submitted",
          "project_phase": "Assigned",
          "preparor": "Victor Joyson D'sa",
          "message": "1",
          "priority": "Low"
      }
  ]
  */
  }

  
}
