import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../rest-api.service';
import {AppModule} from '../app.module';

import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import {Router} from '@angular/router';



import { FingerprintAIO , FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { Storage } from '@ionic/storage';
import { Platform ,AlertController} from '@ionic/angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userPass: any = "";
  user: string = "";
  fop: FingerprintOptions;
  loginresult:number=0;
  showFingerButton = false;
  
  //IDCS variables
  showMFA : boolean = false;
  showMFAcode : boolean = true;
  loginType='1';
  // status :any;
// authentication : any;
MFAcode : any;
MFAmessage : any;
nextAuthFactors : any;
nextOp : any;
requestState : any;
// state : any;

  
  constructor(public appModule: AppModule 
    ,public restApiService: RestApiService 
    ,public loadingController: LoadingController
    ,public toastController: ToastController
    ,public router: Router
    ,private fingerPrint: FingerprintAIO
    ,private storage: Storage
    ,private platform: Platform
    ,public alertCtrl: AlertController ) {  
      
      AppModule.status=0;
      AppModule.state=1;

      this.fop = {
      clientId: 'Fingerprint-Demo',
      clientSecret: 'password', 
      disableBackup: true,  
      localizedFallbackTitle: 'Use Pin', 
      localizedReason: 'Smart Touch' 
    }


  
   }

  

   async  intilaiazelogin() {
    let username;
    let password;
    let userfinger = await this.getValue('userfinger');
    if (userfinger) {

      const FPA = await this.checkFingerPrintAvailabliity();
      if (FPA == 1) {
        username = await this.getValue('username');
        password = await this.getValue('userPass');

        if (username) {

          this.showfin(username, password);
          // if (this.bioType=='face')
          // this.showFaceButton=true;
          // else
           this.showFingerButton=true;
         
        }
        else {

        }
      }

    }
  }

   async getValue( name) {

    return new Promise(resolve => {
      this.storage.get(name).then((val) => {
        resolve(val);
      });

    });
  }

  async checkFingerPrintAvailabliity(): Promise<any> {
    try {
      await this.platform.ready();
      const av = await this.fingerPrint.isAvailable();
      if (av == 'OK' || av == 'finger' || av=='face') {
          // if (av=='face')
          //  this.bioType='face';
        return 1;
      }
      else return 0;
    }
    catch (err) {
      return 0;
    }


  }
  navigat()
  {
// alert ('x');
this.user=null;
this.userPass=null;
    this.router.navigateByUrl('/request-summary'); 
    // alert ('y');
  }
  async showfin(user, pass) {
   
    // alert (user);
    // alert (pass);

    try {
      await this.platform.ready();
      const av = await this.fingerPrint.isAvailable();
      if (av == 'OK' || av == 'finger' || av=='face') {
        const res = await this.fingerPrint.show(this.fop);
       const result = await this.doLogin(user, pass);
console.log('after login 1');
// alert ('r:'+this.loginresult)
       if (this.loginresult == 1)
       {  
        this.navigat();
        
       }
       else
       {
alert ('inside else');

       }
   
        console.log('after login 4');
      }
      else {
        console.log('after login 5');
        this.navigat();
      }
      console.log('after login 6');
    } catch (error) {
      console.log('iside erroorrrr :'+error);
     // AppModule.showMessage(this.alertCtrl,'Error:'+error)
    }


  }

  async showconfirmfinger() {

   
    try {
      await this.platform.ready();
      const av = await this.fingerPrint.isAvailable();
      if (av == 'OK' || av == 'finger' || av=='face') {
        const res = await this.fingerPrint.show(this.fop);

        this.storage.set('userfinger', '1');
     
        this.navigat();

      }
      else {

        this.navigat();
      }



    } catch (error) {
      console.log(error)
      this.navigat();
    }


  }

  setValue(user: string, password: string) {

    this.storage.set('username', user);
    this.storage.set('userPass', password);


  }

  async fingerPrinAction()
{
  

  let username = await this.getValue('username');
  let password = await this.getValue('userPass');

  if (username) {

    this.showfin(username, password);

} 



}

async showConfirm() {
  let message='Do you want to use your Touch ID to login ?';
  // if(this.bioType=='face')
  // message='Do you want to use your Face ID to login ?';


  const confirm = await this.alertCtrl.create({
    header: 'Login',
    subHeader: message,
    buttons: [
      {
        text: 'No',
        handler: () => {
      
          this.navigat();
        
        }
      },
      {
        text: 'Yes',
        handler: () => {

          this.showconfirmfinger();
         
        }
      }
    ]
  });
  confirm.present();
}


  ngOnInit() {
    this.intilaiazelogin();
  }

  async login()
  {
    console.log('inside login a1');
    if (this.user == null)
    {
      alert('Enter username');
      return;
    }
    else if (this.userPass == null)
    {
      alert('Enter password');
      return;
    }

    ////// START IDCS

    if (!this.showMFA)
		{
			this.loginType='1';
    }
    
    /////  END IDCS


     const result = await this.doLogin(this.user, this.userPass);
    // alert (result+'is')
     console.log('result is :' + this.loginresult);

     if (this.loginresult == 1)
     {  
      
              
              const FPA = await this.checkFingerPrintAvailabliity();
              if (FPA == 1) {
              
              //this.showConfirm();
              //  this.showconfirmfinger();
              if (this.platform.is('ios')) {
              this.showconfirmfinger();
            }
            else
            {
              this.showConfirm();
            }
              
              }
              else {
                
                this.navigat();
              }
     }
   
  }

  async doLogin(user,userPass) : Promise<any> {
    this.loginresult=0;
    // alert('x 1');
    // console.log('inside async doLogin()');
        const loading = await this.loadingController.create({
          message:'Authenticating user...'    });
        await loading.present();

//IDCS
const r= await this.intiateAuth(user,userPass,this.MFAcode);

if (AppModule.status !=2)
          {
            loading.dismiss();
            return;
          }

          // idc mfa 
        
        console.log('result is :' + this.loginresult);
//IDCS

        await this.restApiService.doLoginx(user, userPass)
       .subscribe(res => {
          // .then(res => {
            console.log('before login');
            console.log(res);
            console.log('after login');
                if (res[0] == null)
                {
                  console.log('no data');
                  alert('Unable to authenticate..');
                  // alert('x 3');
                  // return;
                }
                 else
                 {
                  // alert('x 4');  
                  if (res[0].Status != 'Y')
                    {
                      this.loginresult=0;
                      loading.dismiss();  
                      alert(res[0].Message);
                      // alert('x 5');
                     return 0;
                    }
                    else{
                      // alert('x 6');
                      this.loginresult=1;
                   
                    
                      this.setValue(user, userPass);
                      // alert('x 7');
                     loading.dismiss();  
                     if (user !=null)
                      AppModule.userName = user.toUpperCase();
                      console.log('dataxx');
                     //resolve(1);
                  
                     this.router.navigateByUrl('/request-summary'); 
                    // alert('x 8');
                    }
                    console.log('there is data');
                 }
             loading.dismiss();
            //  alert('x 9');
          }, err => {
            console.log(err);
            loading.dismiss();
            // alert('x 10...:');
            // alert('x 10:'+err.message);
            return 0;
          });
          console.log('there is data22');
          // alert('x 11');
          return  this.loginresult;
        }

  
        ////////////////////////////IDCS
async intiateAuth(user: string, pa: string , mfa : string): Promise<any> {
  

  if (this.showMFA && !this.MFAcode)
  {
    this.showErrorMFA();
    return;

  }


try {

  await this.restApiService.intiateAuth(user, pa,mfa)

    .then(async data => {
      let resp:any;
      console.log(data);
      resp=data;
     if (resp.status==0)  // error
     {
      // AppModule.showMessage(this.alertCtrl,resp.message);
      alert(resp.message);
     }

     if (resp.status==1 && resp.state==3)
      {
       
        this.MFAmessage=resp.factorMessage ;
       
        AppModule.nextAuthFactors=resp.nextAuthFactors;
        AppModule.nextOp=resp.nextOp;
        AppModule.requestState=resp.requestState;
        AppModule.state=resp.state;
       
        this.showMFA=true;
        

        if (resp.factorValue==0)
        {
          this.showMFAcode=false;
          this.MFAcode=-1;
        }
  
  this.user=user;
this.userPass=pa;
        

      }

      if (resp.status==2 )  // final
      {
        AppModule.status=2;
        AppModule.authentication='Bearer '+resp.token;
      }


    
      
    });

}
catch (error) {

 alert('Error:'+error);
}

}

showErrorMFA() {
  var el;
  el = document.getElementById("MFA");
  // $(el).addClass('error-color');
  }

}
