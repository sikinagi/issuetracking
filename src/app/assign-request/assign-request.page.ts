import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../rest-api.service';
import {AppModule} from '../app.module';

import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-assign-request',
  templateUrl: './assign-request.page.html',
  styleUrls: ['./assign-request.page.scss'],
})
export class AssignRequestPage implements OnInit {

  primaryAssigneeList: any;
  secondaryAssigneeList: any;
  priorityList: any;
  projectPhaseList: any;
  projectTypeList: any;
  
  primaryAssignee: any;
  secondaryAssignee: any;
  primaryStartDate: any;
  projectPriority: any=null;
  projectPhase: any=null;
  projectType: any=null;
  requestDetails: any;
  projectNumber: any
  projectDescription: any;
  projectLongDescription: any;

  constructor(public restApiService: RestApiService 
    ,public loadingController: LoadingController
    ,public toastController: ToastController
    ,public router: Router
    ) {
    this.primaryAssigneeList= AppModule.primaryAssigneeList;
    this.secondaryAssigneeList= AppModule.secondaryAssigneeList;
    this.priorityList= AppModule.priorityList;
    // this.projectPhaseList = AppModule.projectPhaseList;
    // this.projectTypeList = AppModule.projectTypeList;
    this.projectNumber = AppModule.requestDetails.project_number;
    this.projectDescription = AppModule.requestDetails.project_description;
    this.primaryAssignee = AppModule.requestDetails.primary_assignee_id;
    this.secondaryAssignee = AppModule.requestDetails.secondary_assignee_id;
    this.projectLongDescription = AppModule.requestDetails.project_long_description;

    // this.projectType = AppModule.requestDetails.project_type;
    // if(this.projectType == null)
    //   this.projectType  = 'ENHANCEMENT';

    // this.projectPhase = 'Assigned';

    this.projectPriority = AppModule.requestDetails.priority;
    // if(this.projectPriority == null)
    //   this.projectPriority  = 'Normal';
   }

  ngOnInit() {
  }

  AssignRequest()
  {
    if (this.primaryAssignee == null)
    {
      alert('Choose Primary Assignee');
      return;
    }
    else if (this.secondaryAssignee == null)
    {
      alert('Choose Secondary Assignee');
      return;
    }
    this.submitAssignRequest();
  }

  async submitAssignRequest() {
    console.log('inside async getPrimaryAssignee()');
        // const loading = await this.loadingController.create({
        //   message:'Please wait.. submitting request...'    });
        // await loading.present();
        console.log("before invoking ws: ");
        // alert("before invoking ws...");
        await this.restApiService.submitAssignmentRequest(AppModule.requestDetails.project_id
          ,this.primaryAssignee                                                                              
          ,this.secondaryAssignee
          ,AppModule.userName
          ,this.projectType
          ,'Assigned'
          ,this.projectPhase
          ,''
          ,this.projectPriority
          )
        .subscribe(res => {
          // alert("before list");
            console.log('before list');
            console.log(res);
            console.log('after list');
            // alert("after list");

            if (res[0] == null)
            {
              console.log('no data');
              // alert("no data");
              // loading.dismiss();
            }
            else
            {
            // alert(res[0].retStatus);

            if (res[0].retStatus != 'S')
             alert(res[0].retMsg);
            if (res[0].retStatus == 'S')
            {
             alert('Assigned Successfully');
             this.router.navigateByUrl('/request-summary');
            }


            // alert(res[0].retStatus != 'S');
                 }
            // loading.dismiss();
          }, err => {
            // alert('kkkkk');
            alert("error");
            console.log(err);
            // loading.dismiss();
          });
       }
}
