import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRequestPage } from './assign-request.page';

describe('AssignRequestPage', () => {
  let component: AssignRequestPage;
  let fixture: ComponentFixture<AssignRequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignRequestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
